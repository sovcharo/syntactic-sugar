import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.function.BinaryOperator;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Java11 {
    public static void main(String[] args) throws IOException, InterruptedException {
        deprecationsAndRemovals();
        httpClientApi();
        string();
        files();
        optional();
        predicate();
        // now supports Unicode 10
        lambda();
    }

    public static void deprecationsAndRemovals() {
        // JAXB, JAX-WS, JAX-WS annotations - removed
        // Thread - removed methods or marked for removal
        /**
            Thread t = new Thread(() -> {});
            t.countStackFrames();
            t.destroy();
            t.resume();
            t.stop();
            t.stop(Throwable obj);
            t.suspend();
         **/
        // SecurityManager securityManager a couple of methods were removed, none of which used in noah
        // JavaFX moved outside of the JDK
        // no more java applets and java web start, nashorn is deprecated (alternative graal)
    }

    public static void httpClientApi() throws IOException, InterruptedException {
        // supports http2, websockets
        // reactive streams support
        HttpClient client = HttpClient.newHttpClient();

        HttpRequest request = HttpRequest.newBuilder(URI.create(""))
                .GET()
                .build();
        // 1.
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

        // 2.
        CompletableFuture<HttpResponse<String>> resFuture = client.sendAsync(request, HttpResponse.BodyHandlers.ofString());
    }

    public static void string() {
        "sexy string".repeat(12);
        "   ".isBlank(); // true
        "\n\t     text     \u2005".strip(); // text - stripTrailing, stripLeading
        "\n\t     text     \u2005".trim(); //text     \u2005 -  ignores unicode spaces
        Stream<String> lines = "1\n\n\n".lines();
    }

    public static void files() throws IOException {
        String content = Files.readString(Path.of(""));
        content = Files.readString(Path.of(""), Charset.forName("UTF-16"));
        Path file = Files.writeString(Path.of(""), "    some text", StandardOpenOption.APPEND);
        file = Files.writeString(Path.of(""), "    some text", Charset.forName("UTF-16"), StandardOpenOption.APPEND);
    }

    public static void optional() {
        var optional = Optional.ofNullable(null);
        optional.isEmpty();
    }

    public static void predicate() {
        // Predicate::not
       Collection<String> content = Stream
               .of("", "adsa", "more")
               .filter(Predicate.not(String::isBlank))
               .collect(Collectors.toList());
    }

    public static void lambda() {
        // in order to make use of annotations - @Nullable, @Nonnull, etc.
       BinaryOperator<String> operator = (var a, var b) -> a.concat(b);

    }
}
