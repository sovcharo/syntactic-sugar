import java.nio.ByteBuffer;

public class Java13 {
    public static void main(String[] args) {
        byteBuffer();
        switchExpressions(); // this is still a preview feature introduced in java 12
        textBlocks(); // preview feature
        string();
        // widens character support with 4 new scripts
        // introduces new experimental gc: ZGC
    }

    public static void byteBuffer() {
        byte[] src = new byte[5];
        byte[] dst = new byte[5];
        ByteBuffer byteBuffer =  ByteBuffer.wrap(src);
        // new methods
        ByteBuffer byteBuffer1 = byteBuffer.get(3, dst);
        byteBuffer1 = byteBuffer.put(0, src); // index
        byteBuffer1 = byteBuffer.put(0, src); // index
        byteBuffer1 = byteBuffer.put(0, dst, 2, 2); // index, offset and length
        byteBuffer1 = byteBuffer.put(0, src, 2, 3); // index, offset and length
    }

    public static void switchExpressions() {
        int monthNumber = 1;
        String monthName = switch(monthNumber) {
            case 1 -> {
                String month = "January";
                yield  month;
            }
            case 2 -> {
                String month = "February";
                yield month;
            }
            default -> "Unknown";
        };

        String monthName1 = switch(monthNumber) {
            case 1 ->  "January";
            case 2 -> "February";
            default -> "Unknown";
        };
    }

    public static void textBlocks() {
        // 1. normalizes line endings
        // 2. trims non-essential white space (trailing)
        // 3. translates escape sequences
        String json = """
           {
                "firstName" : "Lady",
                "lastName" : "Luck"
           }
        """; // no need to add /n or escape quotes, essential preserves spaces
    }

    public static void string()  {
        String html = "" +
                "<html>\n" +
                "   <head>   </head>\n" +
                "  <body>  %s</body>\n" +
                "</html>";

        // strips accidental space
        html.stripIndent(); // marked as will be removed because it is uncertain whether text blocks and these methods will remain
        html.translateEscapes(); // -- // ---
        html.formatted("first parameter %s"); // -- // --
    }
}
