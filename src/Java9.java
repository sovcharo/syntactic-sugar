
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Clock;
import java.time.Duration;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static  java.util.stream.Collectors.*;

import java.util.function.Consumer;
import java.util.stream.Stream;

public class Java9 {
    public static void main(String[] args) throws IOException {
        migrationIssues();
        collectionFactoryMethods();
        streams();
        collectors();
        optional();
        interfaces();
        tryWithResources();
        betterTypeInference();

        // stop using java.util.Date
        javaTime();
    }

    public static void migrationIssues() {
        // classpath apps are easily migrated without module descriptors
        // it acts as if the classes in the classpath are a part of an unnamed module
    }

    public static void collectionFactoryMethods() {
        // for small collections
        List<Integer> intsList = List.of(1, 2, 3); // returns immutable collection // ImmutableCollections.List1, List2, or ListN types
        Set<Integer> intSet = Set.of(1,2); // null is prohibited, throws NPE
        Map<String, String> stringMapFirstType = Map.of("key1", "value1", "key2", "value2");
        // or
        Map<String, String> stringMapSecondType = Map.ofEntries(Map.entry("key1", "value1"), Map.entry("key2", "value2")); // no nulls for key or value allowed
        // maps and sets order is not guaranteed and is not stable
    }
    public static void streams() {
        // Stream<T> takeWhile(Predicate<? super T> p) - take values until the predicate returns false and lose everything else
        Stream.of(1,2,3).takeWhile(e -> e < 2).collect(toList()); // [1]
        // Stream<T> dropWhile(Predicate<? super T> p) - drop values until the predicate returns false and take everything else
        Stream.of(1,2,3).dropWhile(e -> e < 2).collect(toList()); // [2, 3]
        // static Stream<T> ofNullable(T t) -  for 0 or 1 element stream

        // static Stream<T> iterate(T seed, Predicate<? super T> hasNext, UnaryOperator<T> next) - stop producing values at some point (hasNext rule)
     }

     public static void collectors() {
         Stream<Book> books = Stream.of(new Book());
         // Collectors.filtering(Predicate p, Collector downstreamCollector)

         Map<Set<String>, Set<Book>> booksByAuthors =  books
                 .collect(
                         groupingBy(
                                 Book::getAuthors,
                                 filtering(b -> b.getPrice() > 10, toSet())));
         // Collectors.flatMapping(Function mapper, Collector downstream)
         Map<Double, Set<String>> authorsSellingForPrice = books.collect(
                 groupingBy(
                         Book::getPrice,
                         flatMapping(b -> b.getAuthors().stream(), toSet())));
     }

     public static void optional() {
        Optional<Book> book = Optional.of(new Book());
        // 1. ifPresentOrElse
         Runnable r = () -> {
             // action if not present
         };
         Consumer<Book> consumer =  b -> {
             // action if present
         };
         book.ifPresentOrElse(consumer, r);
         book.ifPresentOrElse(System.out::println, () -> {});
         // 2. or
         book.or(() -> Optional.of(new Book()));
         // 3. stream
         Stream<Book> books = book.stream();
         Stream<Optional<Integer>> optionals = Stream.of(Optional.of(1), Optional.empty(), Optional.of(2));
         Stream<Integer> ints = optionals.flatMap(Optional::stream); // [1, 2]
     }

    interface Test {
        private void someMethod() {
        }

        default void someOtherMethod() {
            someMethod();
        }
    }

     public static void interfaces() {
        Test test = new Test() {
                // interfaces can have private methods
             };
     }

     public static void tryWithResources() throws IOException {
        // no longer needed to declare a new variable for the resource it just has to be an effectively final variable
         FileInputStream fis = new FileInputStream("");
         try (fis) {
             fis.read();
         }
     }

     public static void betterTypeInference() {
        ArrayList<String> list = new ArrayList<>() { // now it's possible to use diamond operators for anonymous classes
            @Override
            public boolean add(String s) {
                return true;
            }
        };
     }

     public static void javaTime() {
        // duration
        Duration.ofDays(1).dividedBy(1);
        Duration.ofDays(1).truncatedTo(ChronoUnit.HOURS);
        // Clock
         Clock.systemUTC();
         // LocalDate
         LocalDate start = LocalDate.of(2017, 12, 1);
         Stream<LocalDate> datesBetween = start.datesUntil(start.plusDays(7));
     }
}

class Book {
    private Set<String> authors;
    private double price;
    private String title;

    public Set<String> getAuthors() {
        return authors;
    }

    public void setAuthors(Set<String> authors) {
        this.authors = authors;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
