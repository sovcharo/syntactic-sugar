public class Java10 {

    public static void main(String[] args) {

        deprecations();
        localVariableTypeInference();
    }

    public static void deprecations() {
        // java.security.acl -> java.security
        // java.security.{Certificate, Identity, IdentityScope, Signer}
        // javax.security.auth.Policy
    }
    public static void localVariableTypeInference() {
        var a = 1;
        // Lambdas must have an explicit target type
        // var p = (String s) -> s.length() > 3; is illegal
        // var a = null is illegal
    }
}
