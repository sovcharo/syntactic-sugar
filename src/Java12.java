import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.NumberFormat;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Java12 {
    public static void main(String[] args) throws IOException {
        string();
        compactNumberFormat();
        teeingCollector();
        files();

        // you are able to use as of this version preview features which may or may not be stable

    }
    public static void string() {
        // raw strings comming in later versions
        "test".indent(2); // adds also a line ending
        "test".transform(string ->  {
            // perform some transformation  actions to produce a new arbitrary object
           return new Object();
        });
    }

    public static void compactNumberFormat() {
        NumberFormat shortNF = NumberFormat.getCompactNumberInstance();
        String shortened = shortNF.format(1000); // 1K according to default locale
        // etc... it is also possible to define your own pattern
    }
    public static void teeingCollector() {
        // now you can apply two collectors at once and then combine their results
       long average = Stream.of(10, 20, 30)
               .collect(
                       Collectors.teeing(
                               Collectors.summarizingInt(Integer::valueOf),
                               Collectors.counting(),
                               (intSummaryStatistics, count) -> intSummaryStatistics.getSum() / count));
    }

    public static void files() throws IOException {
        Files.mismatch(Path.of(""), Path.of("")); // returns location of the first mismatching byte
    }
}
